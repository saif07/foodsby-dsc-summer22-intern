# Foodsby's Data Challenge
##### Proprietary and Confidential
------------

## Included in this repository are several datasets, and a Jupyter Notebook, where you will add your work. Your work must only include your own progress included in your submission, and publicly available Python Packages (e.g. from PyPI)

## Submission instructions

**Please follow all listed steps to ensure a prompt review of your submission by our data hiring team:**

0. We value your time, and would prefer that you only spend a 2-4 hours on the challenge. Completion is not required, what we want to see is a thoughtful and efficient approach to the questions, not perfectedly completed/correct work.
1. Fork the `data-challenge` repository. How to fork on bitbucket Click the plus `+`. Next click `Fork this repo`.
2. In your forked `data-challenge` repo, click on `Settting` and then click on `Users and group access`. Under `Users`, please type in `andrew.evans@foodsby.com` and choose the `Admin` role permission. When done, click `Add`. *This will give us permissions to review your challenge submission when complete.*
3. In the forked `data-challege` project, create a new branch `lastname-firstname`. Work on the assignment and commit your changes to the `lastname-firstname` branch.
4. When complete with the assignment, after having committed all your changes, create a new `Pull Request` in the pull request tab. Add `andrew.evans@foodsby.com` as an assignee. *This will trigger an email notification to review your submission.*
	
* If any questions come up, please send an email to your Foodsby point of contact.
